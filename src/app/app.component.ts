import { Component, OnInit } from '@angular/core';
import { DataService } from './data/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'Sorting-List';

  componentLoaded = false

  constructor(public db: DataService) {}

  ngOnInit(): void {
    this.componentLoaded = true
  }

}
