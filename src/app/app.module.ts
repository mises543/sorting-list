import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MediaListComponent } from './routing/media-list/media-list.component';
import { AddItemComponent } from './routing/media-list/item-form/item-form.component';
import { AppRoutingModule } from './app-routing.module';
import { NavBarComponent } from './routing/nav-bar/nav-bar.component';
import { SharedModule } from './shared/shared.module';
import { ProgressBarComponent } from './routing/progress-bar/progress-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MediaListComponent,
    AddItemComponent,
    ProgressBarComponent,
  ],
  entryComponents: [
    AddItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
