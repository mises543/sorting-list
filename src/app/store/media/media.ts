export interface Media {
    id?: string
    title: string
    category: string
    uploaded: any
}

export namespace MediaOptions {
    export const CATEGORIES = ['All', 'Video', 'Interactive Video', 'Audio', 'Image', 'Document']
    export const SORTS = ['Time', 'Alphabetical']
}