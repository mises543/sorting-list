import { Media } from './media'

export class AddMedia {
    static readonly type = '[Media] Add'
    constructor(public payload: Media) { }
}

export class UpdateMedia {
    static readonly type = '[Media] Update'
    constructor(public payload: Media, public id: string) { }
}

export class GetMedia {
    static readonly type = '[Media] Get'
}

export class DeleteMedia {
    static readonly type = '[Media] Delete'
    constructor(public id: string) { }
}