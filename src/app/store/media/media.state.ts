import { MediaService } from 'src/app/data/media.service'
import { State, Selector, Action, StateContext } from '@ngxs/store'
import { GetMedia, AddMedia, UpdateMedia, DeleteMedia } from './media.actions'
import { tap } from 'rxjs/operators';

export class MediaStateModel {
    items: any[]
}

@State<MediaStateModel>({
    name: 'media',
    defaults: {
        items: [],
    }
})
export class MediaState {

    constructor(private mediaService: MediaService) { }

    @Selector()
    static fetchMediaList(state: MediaStateModel) {
        return state.items;
    }

    @Action(GetMedia)
    getMedia({ getState, patchState }: StateContext<MediaStateModel>) {
        return this.mediaService.getMedia().pipe(tap((result) => {
            result = this.timestampToDate(result)
            let state = getState();
            patchState({ ...state, items: result });
        }));
    }

    @Action(AddMedia)
    addMedia(ctx: StateContext<MediaStateModel>, { payload }: AddMedia) {
        this.mediaService.addMedia(payload)
    }

    @Action(UpdateMedia)
    updateMedia(ctx: StateContext<MediaStateModel>, { payload, id }: UpdateMedia) {
        return this.mediaService.updateMedia(payload, id)
    }

    @Action(DeleteMedia)
    deleteMedia(ctx: StateContext<MediaStateModel>, { id }: DeleteMedia) {
        return this.mediaService.deleteMedia(id)
    }

    timestampToDate(payload: any[]) {
        payload.forEach(item => {
            item.uploaded = item.uploaded.toDate()
            return item
        })
        return payload
    }

}