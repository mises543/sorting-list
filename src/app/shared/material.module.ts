import { NgModule } from '@angular/core';

import {
  MatButtonModule, MatListModule, MatInputModule, MatFormFieldModule, MatSelectModule,
  MatIconModule, MatDatepickerModule, MatNativeDateModule, MatSidenavModule, MatToolbarModule,
  MatDialogModule, MatSnackBarModule, MatProgressBarModule, MatTableModule, MatSortModule
} from '@angular/material';
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDialogModule,
    ScrollingModule,
    MatSnackBarModule,
    MatProgressBarModule,
  ],
  exports: [
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDialogModule,
    ScrollingModule,
    MatSnackBarModule,
    MatProgressBarModule,
  ]
})
export class MaterialModule { }
