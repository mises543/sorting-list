import { NgModule } from '@angular/core';
import { FirebaseModule } from './firebase.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { DataService } from '../data/data.service';
import { MediaService } from '../data/media.service';
import { SnackService } from './snack.service';
import { XsModule } from './xs.module';
import { FilterSortPipe } from './pipes/filter-sort.pipe';

@NgModule({
    imports: [
        FirebaseModule,
        XsModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [
        SnackService,
        DataService,
        MediaService,
    ],
    exports: [
        FirebaseModule,
        XsModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        FilterSortPipe
    ],
    declarations: [FilterSortPipe]
})
export class SharedModule { }
