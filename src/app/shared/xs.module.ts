import { NgModule } from '@angular/core';
import {NgxsModule} from '@ngxs/store';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';
import { MediaState } from '../store/media/media.state';


@NgModule({
    declarations: [],
    imports: [
        NgxsModule.forRoot([
            MediaState
        ]),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsLoggerPluginModule.forRoot(),
    ],
    exports: [
        NgxsModule,
        NgxsReduxDevtoolsPluginModule,
        NgxsLoggerPluginModule,
    ]
})
export class XsModule { }
