import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class SnackService {

    constructor(private snackBar: MatSnackBar) { }

    open(message: string, panelClass?: string, action?: string) {
        this.snackBar.open(message, action ? action : '', panelClass ? { duration: 5000, panelClass: panelClass} : { duration: 5000 })
    }
}