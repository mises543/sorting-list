import { Pipe, PipeTransform } from '@angular/core';

export interface Criteria {
  query: string
  category: string
  defaultCategory: string
  sortBy: string
  ascendingDescending: boolean
}

@Pipe({
  name: 'FilterSort'
})
export class FilterSortPipe implements PipeTransform {

  transform(array: any[], category: string, query: string, sortBy: string, ascendingDescending: boolean): any {
    let newArray = array
    if (category != "All") {
      newArray = this.filterCategory(newArray, category)
    }

    if (query != '') {
      newArray = this.filterTitle(newArray, query)
    }

    if (sortBy != 'Time') {
      newArray = this.sortByAlphabetical(newArray, ascendingDescending)
      // newArray.pop()
    } else {
      newArray = this.sortByTime(newArray, ascendingDescending)
      // newArray.pop()
    }

    console.log(newArray) // output ok on sortBy and ascendingDescending change template not change.
    return newArray;
  }

  filterCategory = (array: any[], filterOption: string) => array.filter(item => item.category == filterOption)

  filterTitle = (array: any[], title: string) => array.filter(item => {
    item.title = item.title.toLowerCase()
    return item.title.indexOf(String(title).toLowerCase()) > -1
  })

  // there is an option to exchange .uploaded ?
  public sortByTime = (data: any[], ascendingDescending: boolean) => data.sort((a: any, b: any) => {
    if (ascendingDescending) {
      return this.getTime(new Date(b.uploaded)) - this.getTime(new Date(a.uploaded))
    } else {
      return this.getTime(new Date(a.uploaded)) - this.getTime(new Date(b.uploaded))
    }
  })

  private getTime(date?: Date) {
    return date != null ? date.getDate() : 0;
  }

  // there is an option to exchange .title ?
  public sortByAlphabetical = (data: any[], ascendingDescending: boolean) => data.sort((a: any, b: any) => {
    if (ascendingDescending) {
      if (a.title < b.title) return -1
      if (a.title > b.title) return 1
    } else {
      if (a.title < b.title) return 1
      if (a.title > b.title) return -1
    }
    return 0
  })


}
