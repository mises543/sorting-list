import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  active: boolean = false
  subs: Subscription

  isHandset$: Observable<boolean> = this.breakPointObserver.observe(['(max-width: 750px)'])
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakPointObserver: BreakpointObserver, private router: Router) { }

  ngOnInit(): void {
    this.trackRoute()
  }

  trackRoute() {
    this.router.url === '/' ? this.active = true : this.active = false
    this.subs = this.router.events.subscribe(val => {
      if(val instanceof NavigationEnd){
        if(val.urlAfterRedirects === '/'){
          this.active = true
        } else {
          this.active = false
        }
      }
    })
  }

  ngOnDestroy(): void {
    this.subs ? this.subs.unsubscribe : null
  }

  async logOut() {
    // try {
    //   await this.auth.logOut()
    //   this.snack.open('Log out success.', '', 'success')
    // } catch(err) {
    //   this.snack.open(err.message, '', 'error')
    // } finally {
    //   this.auth.isLoading = false
    // }
  }

}
