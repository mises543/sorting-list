import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SnackService } from 'src/app/shared/snack.service';
import { Media, MediaOptions } from 'src/app/store/media/media';
import { Store } from '@ngxs/store';
import { AddMedia, UpdateMedia } from 'src/app/store/media/media.actions';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss']
})
export class AddItemComponent implements OnInit {

  categories = MediaOptions.CATEGORIES

  uploaded = new FormControl(new Date(), [Validators.required])
  title = new FormControl('', [Validators.minLength(5), Validators.required])
  category = new FormControl('', [Validators.required])

  constructor(private store: Store,
    private snackBar: SnackService,
    public dialogRef: MatDialogRef<AddItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data?: any) {}

  ngOnInit(): void {
    if(this.data) {
      this.title.setValue(this.data.title)
      this.category.setValue(this.data.category)
      this.uploaded.setValue(this.data.uploaded)
    } else {
      this.uploaded.setValue(new Date())
    }
  }

  async onAdd() {
    if (this.title.valid && this.category.valid && this.uploaded.valid) {
      try {
        const media: Media = {
          title: this.title.value,
          category: this.category.value,
          uploaded: this.uploaded.value,
        }
        this.store.dispatch(new AddMedia(media))
        this.snackBar.open("Media item added success!", "success")
        this.dialogRef.close()
      } catch (err) {
        this.snackBar.open(err.message, "error")
      }
    }
  }

  async onUpdate() {
    if (this.title.valid && this.category.valid && this.uploaded.valid) {
      try {
        const media: Media = {
          title: this.title.value,
          category: this.category.value,
          uploaded: this.uploaded.value,
        }
        this.store.dispatch(new UpdateMedia(media, this.data.id))
        this.snackBar.open("Media item added success!", "success")
        this.dialogRef.close()
      } catch (err) {
        this.snackBar.open(err.message, "error")
      }
    }
  }

  onCancel() {
    this.dialogRef.close()
  }

  getErrorCategory() {
    return this.category.hasError('required') ? 'You must pick a category!' : '';
  }

  getErrorTitle() {
    return this.title.hasError('required') ? 'You must enter a title!' :
      this.title.hasError('minlength') ? 'Please insert minimum 5 characters!' : '';
  }

}
