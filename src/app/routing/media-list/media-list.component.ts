import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { AddItemComponent } from './item-form/item-form.component';
import { MediaOptions } from '../../store/media/media';
import { SnackService } from '../../shared/snack.service';
import { Select, Store } from '@ngxs/store';
import { MediaState, MediaStateModel } from 'src/app/store/media/media.state';
import { GetMedia, DeleteMedia } from 'src/app/store/media/media.actions';

@Component({
  selector: 'app-media-list',
  templateUrl: './media-list.component.html',
  styleUrls: ['./media-list.component.scss']
})
export class MediaListComponent implements OnInit {

  @Select(MediaState.fetchMediaList) media$: Observable<MediaStateModel>

  displayedColumns: string[] = ['title', 'category', 'uploaded', 'edit', 'delete'];

  CATEGORIES = MediaOptions.CATEGORIES
  SORTS = MediaOptions.SORTS

  query = ''
  category = MediaOptions.CATEGORIES[0]
  sortBy = MediaOptions.SORTS[0]
  ascendingDescending = true

  constructor(private store: Store, private snackBar: SnackService, private dialog: MatDialog) { }

  async ngOnInit() {
    this.store.dispatch(new GetMedia())
  }

  onSearchChange(keyWord: string) {
    this.query = keyWord
  }

  onDescendingChange() {
    this.ascendingDescending = !this.ascendingDescending
  }

  clearFilters() {
    this.query = ''
    this.category = MediaOptions.CATEGORIES[0]
    this.sortBy = MediaOptions.SORTS[0]
    this.ascendingDescending = true
  }

  async deleteItem(id: string) {
    try {
      await this.store.dispatch(new DeleteMedia(id))
      this.snackBar.open("Delete cuccess!", "success")
    } catch (err) {
      this.snackBar.open(err.message, 'error')
    }
  }

  editItem(payload: any) {
    this.dialog.open(AddItemComponent,
      { width: '500px', data: payload });
  }

  async openAddItem() {
    this.dialog.open(AddItemComponent,
      { width: '500px' });
  }

}
