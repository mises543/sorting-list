import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MediaListComponent } from './routing/media-list/media-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/library', pathMatch: 'full'},
  { path: 'library', component: MediaListComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
