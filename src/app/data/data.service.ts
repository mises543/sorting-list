import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  assignId = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;
    return object;
  }

  constructor(private db: AngularFirestore) { }

  fetchItems(path: string) {
    return this.db.collection(path).snapshotChanges().pipe(
      map(data => data.map(this.assignId)),
      catchError(err => { return this.errorHandler(err) })
    )
  }

  addItem(payload: any, path: string) {
    return this.db.collection(path).add(payload).catch(err => { return this.errorHandler(err) })
  }

  updateItem(payload: any, id: string, path: string) {
    return this.db.doc(path + '/' + id).update(payload).catch(err => { return this.errorHandler(err) })
  }

  deleteItem(id: string, path: string) {
    return this.db.doc(path + '/' + id).delete().catch(err => { return this.errorHandler(err) })
  }

  errorHandler(err: any) {
    console.log(err.message)
    return err
  }

}
