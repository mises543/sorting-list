import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { Observable } from 'rxjs';
import { Media } from '../store/media/media';

@Injectable({
    providedIn: 'root'
})
export class MediaService {

    path = "media"

    constructor(public db: DataService) { }

    getMedia() {
        return this.db.fetchItems(this.path) as Observable<Media[]>
    }

    addMedia(payload: any) {
        return this.db.addItem(payload, this.path)
    }

    updateMedia(payload: Media, id: string) {
        return this.db.updateItem(payload, id, this.path)
    }

    deleteMedia(id: string) {
        return this.db.deleteItem(id, this.path)
    }

}
